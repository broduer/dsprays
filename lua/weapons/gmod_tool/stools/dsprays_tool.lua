
-- Copyright (C) 2020-2021 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

TOOL.Category = 'Construction'
TOOL.Name = 'Posters'

TOOL.ClientConVar['label'] = ''
TOOL.ClientConVar['url'] = ''
TOOL.ClientConVar['rating'] = '0'
TOOL.ClientConVar['type'] = '0'
TOOL.ClientConVar['size'] = '50'
TOOL.ClientConVar['dont_collide'] = '0'
TOOL.ClientConVar['dont_move'] = '0'

CreateConVar('sbox_maxdsprays_posters', '10', {FCVAR_NOTIFY, FCVAR_REPLICATED}, 'Maximum amount of posters a player can put down')

local TOOL = TOOL

if CLIENT then
	TOOL.Information = {
		{name = 'left'},
		{name = 'right'},
	}

	function TOOL:BuildCPanel()
		if not IsValid(self) then return end

		local manager = vgui.Create('ControlPresets', self)
		self:AddItem(manager)

		for k, v in pairs(TOOL.ClientConVar) do
			manager:AddConVar('dsprays_tool_' .. k)
		end

		manager:SetPreset('dsprays_tool')

		self:TextEntry('gui.dsrpays.main.labels.label', 'dsprays_tool_label')
		self:TextEntry('gui.dsrpays.main.labels.url', 'dsprays_tool_url')

		local srating = self:ComboBox('gui.dsrpays.rating.rating', 'dsprays_tool_rating')
		local stype = self:ComboBox('gui.dsrpays.main.types.type', 'dsprays_tool_type')

		srating:SetSortItems(false)
		stype:SetSortItems(false)

		for i, label in ipairs(DSprays.RatingList) do
			srating:AddChoice(label, tostring(i))
		end

		srating:ChooseOptionID(ConVar('dsprays_tool_rating'):GetInt():clamp(1, #DSprays.RatingList))

		for i, label in ipairs(DSprays.SprayTypeName) do
			stype:AddChoice(label, tostring(i))
		end

		stype:ChooseOptionID(ConVar('dsprays_tool_type'):GetInt():clamp(1, #DSprays.SprayTypeName))

		self:NumSlider('gui.dsrpays.main.spray_size', 'dsprays_tool_size', 10, 100, 0)

		self:CheckBox('gui.dsrpays.tool.dont_move', 'dsprays_tool_dont_move')
		self:CheckBox('gui.dsrpays.tool.dont_collide', 'dsprays_tool_dont_collide')
	end
end

function TOOL:LeftClick(tr)
	local ply = self:GetOwner()
	local url = self:GetClientInfo('url', ''):trim()
	if not DSprays.ValidURL(url) then return false end

	local rating = self:GetClientNumber('rating', 0):clamp(0, 3)
	local type = self:GetClientNumber('type', 0):clamp(DSprays.TYPE_STATIC_PICTURE, DSprays.TYPE_DYNAMIC_VIDEO)
	local size = self:GetClientNumber('size', 50):clamp(10, 100):round()

	local isnew = not IsValid(tr.Entity) or tr.Entity:GetClass() ~= 'dspray_poster' or tr.Entity:GetSpraySteamID() ~= (ply:SteamID() or 'STEAM_0:0:0') and (not tr.Entity.CPPIGetOwner or tr.Entity:CPPIGetOwner() ~= ply)
	local ent

	if isnew then
		if not ply:CheckLimit('dsprays_posters') then return false end

		if CLIENT then return true end

		ent = ents.Create('dspray_poster')
		ent:SetPos(tr.HitPos + tr.HitNormal * 3)

		local ang = tr.HitNormal:Angle()
		ang:RotateAroundAxis(ang:Up(), 90)
		ang:RotateAroundAxis(ang:Forward(), 90)
		ent:SetAngles(ang)

		-- singleplayer
		ent:SetSpraySteamID(ply:SteamID() or 'STEAM_0:0:0')
	elseif CLIENT then
		return true
	else
		ent = tr.Entity
	end

	ent:SetSprayLabel(self:GetClientInfo('label', ''):trim())
	ent:SetSprayURL(url)
	ent:SetSprayType(type)
	ent:SetSpraySize(size)
	-- ent:SetSpraySize(48)
	-- ent:SetModelScale(size / 50)
	ent:SetSprayRating(rating)
	ent:SetSprayDontCollide(tobool(self:GetClientNumber('dont_collide')))
	ent:SetSprayDontMove(tobool(self:GetClientNumber('dont_move')))

	if isnew then
		ent:Spawn()

		if ent.CPPISetOwner then
			ent:CPPISetOwner(ply)
		end

		ply:AddCleanup('dsprays_posters', ent)
		ply:AddCount('dsprays_posters', ent)

		undo.Create('DSprays Poster')
		undo.AddEntity(ent)

		if not tobool(self:GetClientNumber('dont_move')) then
			if IsValid(tr.Entity) and not tr.Entity:IsRagdoll() and util.IsValidPhysicsObject(tr.Entity, tr.PhysicsBone) then
				local weld = constraint.Weld(tr.Entity, ent, 0, tr.PhysicsBone, 0, true)

				if weld then
					ent:SetIsMovable(true)
					ply:AddCleanup('constraints', weld)
				end
			end
		end

		undo.SetPlayer(ply)
		undo.Finish()

		local phys = ent:GetPhysicsObject()

		if IsValid(phys) then
			phys:EnableMotion(false)
		end
	else
		ent:UpdatePhysics()
		ent:MarkDirty()
	end

	return true
end

if SERVER and game.SinglePlayer() then
	net.pool('dsprays_tool_copy')
elseif CLIENT then
	net.Receive('dsprays_tool_copy', function()
		local ent = net.ReadEntity()

		RunConsoleCommand('dsprays_tool_label', ent:GetSprayLabel())
		RunConsoleCommand('dsprays_tool_url', ent:GetSprayURL())
		RunConsoleCommand('dsprays_tool_type', ent:GetSprayType():tostring())
		RunConsoleCommand('dsprays_tool_size', ent:GetSpraySize():tostring())
		-- RunConsoleCommand('dsprays_tool_size', math.floor(ent:GetModelScale() * 50):tostring())
		RunConsoleCommand('dsprays_tool_rating', ent:GetSprayRating():tostring())
		RunConsoleCommand('dsprays_tool_dont_collide', ent:GetSprayDontCollide() and '1' or '0')
		RunConsoleCommand('dsprays_tool_dont_move', ent:GetSprayDontMove() and '1' or '0')
	end)
end

function TOOL:RightClick(tr)
	if not IsValid(tr.Entity) or tr.Entity:GetClass() ~= 'dspray_poster' then return false end

	if CLIENT then
		local ent = tr.Entity

		RunConsoleCommand('dsprays_tool_label', ent:GetSprayLabel())
		RunConsoleCommand('dsprays_tool_url', ent:GetSprayURL())
		RunConsoleCommand('dsprays_tool_type', ent:GetSprayType():tostring())
		RunConsoleCommand('dsprays_tool_size', ent:GetSpraySize():tostring())
		-- RunConsoleCommand('dsprays_tool_size', math.floor(ent:GetModelScale() * 50):tostring())
		RunConsoleCommand('dsprays_tool_rating', ent:GetSprayRating():tostring())
		RunConsoleCommand('dsprays_tool_dont_collide', ent:GetSprayDontCollide() and '1' or '0')
		RunConsoleCommand('dsprays_tool_dont_move', ent:GetSprayDontMove() and '1' or '0')

		return true
	end

	if not game.SinglePlayer() then return true end

	net.Start('dsprays_tool_copy')
	net.WriteEntity(tr.Entity)
	net.Send(self:GetOwner())

	return true
end

hook.Add('DLib.TranslationsReloaded', 'DSprays Reload Tool Translations', function()
	DLib.I18n.RegisterProxy('tool.dsprays_tool.name', 'gui.tool.dsprays_tool.name')
	DLib.I18n.RegisterProxy('tool.dsprays_tool.desc', 'gui.tool.dsprays_tool.desc')
	DLib.I18n.RegisterProxy('tool.dsprays_tool.left', 'gui.tool.dsprays_tool.left')
	DLib.I18n.RegisterProxy('tool.dsprays_tool.right', 'gui.tool.dsprays_tool.right')
	DLib.I18n.RegisterProxy('Undone_DSprays Poster', 'gui.undone.dsprays_poster')
end)
