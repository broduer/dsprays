
-- Copyright (C) 2020-2021 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

local DSprays = DSprays

CreateConVar('dsprays_sprayurl', '', {FCVAR_ARCHIVE, FCVAR_USERINFO}, 'Spray URL')
CreateConVar('dsprays_sprayurl_rating', '1', {FCVAR_ARCHIVE, FCVAR_USERINFO}, 'Spray URL rating (1 - safe, 2 - suggestive, 3 - ques., 4 - explicit)')

file.mkdir('dsprays')
file.mkdir('dsprays/backup')

if file.Exists('gpf', 'DATA') then
	for i, filename in ipairs(file.Find('gpf/*', 'DATA')) do
		file.Rename('gpf/' .. filename, 'dsprays/' .. filename)
	end

	file.Delete('gpf')
end

local BackupAndWrite
local Backup

do
	local exists = {}

	local function mkdir(path)
		if exists[path] then return end
		exists[path] = true
		file.mkdir('dsprays/backup/' .. path)
	end

	function Backup(path)
		if file.Exists('dsprays/' .. path, 'DATA') then
			local a, b, c, d = os.date('%Y'), os.date('%Y/%m'), os.date('%Y/%m/%d'), os.date('%H_%M_%S_')
			mkdir(a)
			mkdir(b)
			mkdir(c)
			file.Rename('dsprays/' .. path, 'dsprays/backup/' .. c .. '/' .. d .. path)
		end
	end

	function BackupAndWrite(path, data)
		Backup(path)
		file.Write('dsprays/' .. path, data)
	end
end

if not DSprays.SaveData then
	DSprays.SaveData = {
		meta = {
			next_id = 1,

			list = {
				main = {
					name = 'gui.dsrpays.misc.main_gallery'
				}
			}
		},

		data = {
			main = {

			}
		},
	}

	if file.Exists('dsprays/meta.json', 'DATA') then
		DSprays.SaveData.meta = util.JSONToTable(file.Read('dsprays/meta.json', 'DATA'))
	else
		BackupAndWrite('meta.json', util.TableToJSON(DSprays.SaveData.meta, true))
	end

	for name, metadata in pairs(DSprays.SaveData.meta.list) do
		if file.Exists('dsprays/data_' .. name .. '.json', 'DATA') then
			DSprays.SaveData.data[name] = util.JSONToTable(file.Read('dsprays/data_' .. name .. '.json', 'DATA'))
		end
	end
end

if not DSprays.SaveData.meta then
	DSprays.SaveData.meta = {
		next_id = 1,

		list = {
			main = {
				name = 'gui.dsrpays.misc.main_gallery'
			}
		}
	}
end

if not DSprays.SaveData.meta.list then DSprays.SaveData.meta.list = {} end
if not DSprays.SaveData.meta.list.main then DSprays.SaveData.meta.list.main = {main = true} end
DSprays.SaveData.meta.list.main.name = 'gui.dsrpays.misc.main_gallery'
DSprays.SaveData.meta.list.main.hide = false

function DSprays.MainFromData(data)
	local main

	for i, data in ipairs(data) do
		if data.main and not data.hide and DSprays.ValidURL(data.url) then
			return i, data
		end
	end

	for i, data in ipairs(data) do
		if not data.hide and DSprays.ValidURL(data.url) then
			return i, data
		end
	end
end

function DSprays.CheckEmpty(value)
	return value.url:trim() == '' and value.label:trim() == ''
end

for index, list in pairs(DSprays.SaveData.data) do
	for i = #list, 1, -1 do
		if DSprays.CheckEmpty(list[i]) then
			table.remove(list, i)
		end
	end
end

local check_empty = DSprays.CheckEmpty

function DSprays.MetaDataUpdated()
	local write = util.TableToJSON(DSprays.SaveData.meta, true)
	BackupAndWrite('meta.json', write)
	return write
end

function DSprays.DeleteDataFile(name)
	Backup('data_' .. name .. '.json')
	-- file.Delete('dsprays/data_' .. name .. '.json')
end

function DSprays.SaveDataUpdated(index)
	if not index then
		for k in pairs(DSprays.SaveData.data) do
			DSprays.SaveDataUpdated(k)
		end

		return
	end

	if not DSprays.SaveData.data[index] then return end

	local values = {}

	for i, data in ipairs(DSprays.SaveData.data[index]) do
		if not check_empty(data) then
			table.insert(values, data)
		end
	end

	BackupAndWrite('data_' .. index .. '.json', util.TableToJSON(values, true))
end

function DSprays.OpenGUI(index)
	if IsValid(DSprays.MainWindow) then
		DSprays.MainWindow:SetVisible(true)
		DSprays.MainWindow:MakePopup()
		DSprays.MainWindow:Center()
		return
	end

	DSprays.MainWindow = vgui.Create('DLib_Window')
	local self = DSprays.MainWindow

	self:MakePopup()
	self:SetTitle('gui.dsrpays.main.title')
	self:SetSize(math.max(1000, ScrW() * 0.8), math.max(600, ScrH() * 0.8))
	self:Center()

	local sheet = vgui.Create('DPropertySheet', self)
	sheet:Dock(FILL)

	local canvasParent = vgui.Create('EditablePanel', sheet)
	local canvasButtons = vgui.Create('DSprays_MetaList', canvasParent)
	local canvasRows = vgui.Create('EditablePanel', canvasParent)

	local canvasSettings = vgui.Create('DScrollPanel', sheet)
	local canvasSettingsC = canvasSettings:GetCanvas()

	canvasButtons:Dock(LEFT)
	canvasButtons:SetWide(200)
	canvasButtons:SetCanvas(canvasRows)

	canvasButtons:SetZPos(0)
	canvasRows:SetZPos(1)
	canvasRows:Dock(FILL)

	sheet:AddSheet('gui.dsrpays.tabs.galleries', canvasParent)
	sheet:AddSheet('gui.dsrpays.tabs.settings', canvasSettings)

	local labels = {}

	do
		local parent = vgui.Create('EditablePanel', canvasSettingsC)
		parent:Dock(TOP)
		parent:DockMargin(5, 5, 5, 5)

		local label = vgui.Create('DLabel', parent)
		label:Dock(LEFT)
		label:SetZPos(0)
		label:SetText('gui.dsrpays.settings.rating')
		table.insert(labels, label)

		label:SizeToContents()
		label:SetWide(label:GetWide() + 20)

		local box = vgui.Create('DComboBox', parent)
		box:Dock(FILL)
		box:SetZPos(1)

		box:SetSortItems(false)

		for i, label in ipairs(DSprays.RatingList) do
			box:AddChoice(label, tostring(i))
		end

		function box:OnSelect(index, value, data)
			RunConsoleCommand('dsprays_rating', data)
		end

		box:ChooseOptionID(DSprays.dsprays_rating:GetInt():clamp(1, #DSprays.RatingList))
	end

	do
		local parent = vgui.Create('EditablePanel', canvasSettingsC)
		parent:Dock(TOP)
		parent:DockMargin(5, 5, 5, 5)

		local label = vgui.Create('DLabel', parent)
		label:Dock(LEFT)
		label:SetZPos(0)
		label:SetText('gui.dsrpays.settings.size')
		table.insert(labels, label)

		label:SizeToContents()
		label:SetWide(label:GetWide() + 20)

		local box = vgui.Create('DComboBox', parent)
		box:Dock(FILL)
		box:SetZPos(1)

		box:SetSortItems(false)

		for index, size in SortedPairs(DSprays.TextureSizeMap) do
			box:AddChoice(string.format('%dpx x %dpx (%s)', size, size, DLib.I18n.FormatAnyBytesLong(size * size)), index)
		end

		function box:OnSelect(index, value, data)
			RunConsoleCommand('dsprays_texture_size', data)
		end

		box:ChooseOptionID(DSprays.dsprays_texture_size:GetInt():clamp(-4, 2) + 5)
	end

	do
		local parent = vgui.Create('EditablePanel', canvasSettingsC)
		parent:Dock(TOP)
		parent:DockMargin(5, 5, 5, 5)

		local label = vgui.Create('DLabel', parent)
		label:Dock(LEFT)
		label:SetZPos(0)
		label:SetText('gui.dsrpays.settings.size_video')
		table.insert(labels, label)

		label:SizeToContents()
		label:SetWide(label:GetWide() + 20)

		local box = vgui.Create('DComboBox', parent)
		box:Dock(FILL)
		box:SetZPos(1)

		box:SetSortItems(false)

		for index, size in SortedPairs(DSprays.TextureSizeMap) do
			box:AddChoice(string.format('%dpx x %dpx (%s)', size, size, DLib.I18n.FormatAnyBytesLong(size * size * 4)), index)
		end

		function box:OnSelect(index, value, data)
			RunConsoleCommand('dsprays_video_size', data)
		end

		box:ChooseOptionID(DSprays.dsprays_video_size:GetInt():clamp(-4, 2) + 5)
	end

	do
		local parent = vgui.Create('EditablePanel', canvasSettingsC)
		parent:Dock(TOP)
		parent:DockMargin(5, 5, 5, 5)

		local label = vgui.Create('DLabel', parent)
		label:Dock(LEFT)
		label:SetZPos(0)
		label:SetText('gui.dsrpays.settings.hide_everything')
		table.insert(labels, label)

		label:SizeToContents()
		label:SetWide(label:GetWide() + 20)
		parent:SetTall(parent:GetTall():max(label:GetTall()))

		local box = vgui.Create('DCheckBox', parent)
		box:Dock(LEFT)
		box:SetZPos(1)

		box:SetConVar('dsprays_hide_everything')
	end

	do
		local parent = vgui.Create('EditablePanel', canvasSettingsC)
		parent:Dock(TOP)
		parent:DockMargin(5, 5, 5, 5)

		local label = vgui.Create('DLabel', parent)
		label:Dock(LEFT)
		label:SetZPos(0)
		label:SetText('gui.dsrpays.settings.hide_unmatching')
		table.insert(labels, label)

		label:SizeToContents()
		label:SetWide(label:GetWide() + 20)
		parent:SetTall(parent:GetTall():max(label:GetTall()))

		local box = vgui.Create('DCheckBox', parent)
		box:Dock(LEFT)
		box:SetZPos(1)

		box:SetConVar('dsprays_hide_unmatching')
	end

	do
		local parent = vgui.Create('EditablePanel', canvasSettingsC)
		parent:Dock(TOP)
		parent:DockMargin(5, 5, 5, 5)

		local label = vgui.Create('DLabel', parent)
		label:Dock(LEFT)
		label:SetZPos(0)
		label:SetText('gui.dsrpays.settings.do_not_animate')
		table.insert(labels, label)

		label:SizeToContents()
		label:SetWide(label:GetWide() + 20)
		parent:SetTall(parent:GetTall():max(label:GetTall()))

		local box = vgui.Create('DCheckBox', parent)
		box:Dock(LEFT)
		box:SetZPos(1)

		box:SetConVar('dsprays_animated')
	end

	do
		local parent = vgui.Create('EditablePanel', canvasSettingsC)
		parent:Dock(TOP)
		parent:DockMargin(5, 5, 5, 5)

		local label = vgui.Create('DLabel', parent)
		label:Dock(LEFT)
		label:SetZPos(0)
		label:SetText('gui.dsrpays.settings.bypass_cache')
		table.insert(labels, label)

		label:SizeToContents()
		label:SetWide(label:GetWide() + 20)
		parent:SetTall(parent:GetTall():max(label:GetTall()))

		local box = vgui.Create('DCheckBox', parent)
		box:Dock(LEFT)
		box:SetZPos(1)

		box:SetConVar('dsprays_bypass_cache')
	end

	do
		local parent = vgui.Create('EditablePanel', canvasSettingsC)
		parent:Dock(TOP)
		parent:DockMargin(5, 5, 5, 5)

		local label = vgui.Create('DLabel', parent)
		label:Dock(LEFT)
		label:SetZPos(0)
		label:SetText('gui.dsrpays.settings.fullbright')
		table.insert(labels, label)

		label:SizeToContents()
		label:SetWide(label:GetWide() + 20)

		local box = vgui.Create('DCheckBox', parent)
		box:Dock(LEFT)
		box:SetZPos(1)

		box:SetConVar('dsprays_fullbright')
	end

	do
		local label = vgui.Create('DLabel', canvasSettingsC)
		label:Dock(TOP)
		label:DockMargin(5, 5, 5, 5)
		label:SetText('gui.dsprays.cache_size', DLib.I18n.FormatAnyBytesLong(DSprays.CacheManager:TotalSize()))
	end

	do
		local label = vgui.Create('DButton', canvasSettingsC)
		label:Dock(TOP)
		label:DockMargin(5, 5, 5, 5)
		label:SetText('gui.dsrpays.settings.reload_materials')

		label:SizeToContents()
		label:SetWide(label:GetWide() + 20)
		label:SetConsoleCommand('dlib_reload_materials')
	end

	local maxsize = labels[1]:GetWide()

	for i = 2, #labels do
		maxsize = maxsize:max(labels[i]:GetWide())
	end

	for i = 1, #labels do
		labels[i]:SetWide(maxsize + 20)
	end

	canvasButtons:SetupData(DSprays.SaveData.meta, DSprays.SaveData.data)

	if index then
		canvasButtons:OpenCanvas(index)
	end

	function canvasButtons:OnRowUpdate(canvas)
		if canvas:GetDataIndex() ~= 'main' then return end

		local values = canvas:GetNonEmptyValues()

		if #values ~= 0 then
			local main

			for i, data in ipairs(values) do
				if data.main then
					main = data
					break
				end
			end

			if not main then
				main = select(2, next(values))
			end

			if not main then return end

			RunConsoleCommand('dsprays_sprayurl', main.url)
			RunConsoleCommand('dsprays_sprayurl_rating', tostring(main.rating or 1))
		else
			RunConsoleCommand('dsprays_sprayurl', '')
			RunConsoleCommand('dsprays_sprayurl_rating', '1')
		end
	end

	function canvasButtons:OnDataUpdate(index)
		timer.Create('DSprays.SaveDataUpdated_' .. index, 1, 1, DSprays.SaveDataUpdated:Wrap(index))
	end

	canvasButtons.OnTopDataUpdate = canvasButtons.OnDataUpdate

	--[[
	function canvasButtons:OnRootDataUpdate()
		DSprays.SaveDataUpdated()
	end
	]]

	function canvasButtons:OnDataDelete(index)
		DSprays.DeleteDataFile(index)
	end

	function canvasButtons:OnRootMetaUpdate()
		timer.Create('DSprays.MetaDataUpdated', 1, 1, DSprays.MetaDataUpdated)
	end

	canvasButtons.OnMetaUpdate = canvasButtons.OnRootMetaUpdate
end

concommand.Add('dsprays_gui', DSprays.OpenGUI)
