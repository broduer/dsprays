
-- Copyright (C) 2020-2021 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

local DSprays = _G.DSprays
local Promise = DLib.Promise
local dsprays_bypass_cache = CreateConVar('dsprays_bypass_cache', '0', {FCVAR_ARCHIVE}, 'Do not load files from cache. Cache files are still written to disk.')

DSprays.CacheManager = DLib.CacheManager('dsprays_cache', 512 * 0x00100000, 'vtf')
DSprays.CacheManager:AddCommands()
DSprays.CacheManager:SetConVar()

cvars.AddChangeCallback('dsprays_bypass_cache', function()
	hook.Run('DSprays_BypassCacheChanged', dsprays_bypass_cache:GetBool())
end, 'DSprays')

DSprays.PendingTextures = DSprays.PendingTextures or {}
DSprays.PendingTexturesVideo = DSprays.PendingTexturesVideo or {}
DSprays.PendingTexturesDynamic = DSprays.PendingTexturesDynamic or {}
DSprays.TextureCache = DSprays.TextureCache or {}
DSprays.TextureCacheVideo = DSprays.TextureCacheVideo or {}
DSprays.TextureCacheDynamic = DSprays.TextureCacheDynamic or {}

local function HasCache(key)
	return DSprays.CacheManager:Has(key)
end

local function GetCache(key)
	local get = DSprays.CacheManager:HasGet(key)

	if not get then
		return
	end

	return '../data/' .. get
end

local function SetCache(key, value)
	DSprays.CacheManager:Set(key, value)
end

local function InvalidateMaterialCache()
	DSprays.TextureCache = {}
	DSprays.PendingTextures = {}

	for index, data in pairs(DSprays.PendingTexturesVideo) do
		if IsValid(data.panel) then
			data.panel:Remove()
		end
	end

	for index, data in pairs(DSprays.TextureCacheVideo) do
		if IsValid(data.panel) then
			data.panel:Remove()
		end
	end

	DSprays.PendingTexturesVideo = {}
	DSprays.TextureCacheVideo = {}
end

local function HTMLPageVideo(url, width, height)
	-- Fuck Awesomium this time
	return [[
		<html>
			<head>
				<style>
					html, body, #imgdiv {
						background: transparent;
						margin: 0;
						padding: 0;
						overflow: hidden;
					}

					#mainimage {
						max-width: ]] .. width ..[[px;
						max-height: ]] .. height ..[[px;
					}

					#imgdiv {
						width: ]] .. width ..[[px;
						height: ]] .. height ..[[px;
						text-align: center;
					}
				</style>
				<script>
					// Fuck Awesomium this time
					window.addEventListener('load', () => {
						const height = ]] .. height ..[[;
						const img = document.getElementById('mainimage');
						let setup = false;

						img.addEventListener('loadeddata', () => {
							setInterval(() => {
								const mult = (img.videoWidth < img.videoHeight) ? img.videoWidth / img.videoHeight : img.videoHeight / img.videoWidth;
								img.style.setProperty('margin-top', height * (1 - mult) / 2);

								if (!setup) {
									console.log('SETUP');
									setup = true;
								}
							}, 400);
						});
					});
				</script>
			</head>
			<body>
				<div id='imgdiv'>
					<video id='mainimage' autoplay loop muted preload="metadata">
						<source src="]] .. url
							:replace(' ', '%20')
							:replace('"', '%22')
							:replace("'", '%27')
							:replace('#', '%23')
							:replace('<', '%3C')
							:replace('=', '%3D'):replace('>', '%3E') ..[[">
					</video>
				</div>
			</body>
		</html>
	]]
end

local function HTMLPage(url, width, height)
	return [[<html>
				<head>
					<style>
						html, body {
							background: transparent;
							margin: 0;
							padding: 0;
							overflow: hidden;
						}

						#mainimage {
							max-width: ]] .. width ..[[;
							height: auto;
							width: 100%;
							margin: 0;
							padding: 0;
							max-height: ]] .. height ..[[;
						}

						#imgdiv {
							width: ]] .. width ..[[;
							height: ]] .. height ..[[;
							overflow: hidden;
							margin: 0;
							padding: 0;
							text-align: center;
						}
					</style>
					<script>
						window.onload = function() {
							// var and functions because awesomium
							var img = document.getElementById('mainimage');

							if (img.naturalWidth < img.naturalHeight) {
								img.style.setProperty('height', '100%');
								img.style.setProperty('width', 'auto');
							}

							img.style.setProperty('margin-top', (]] .. height ..[[ - img.height) / 2);

							setInterval(function() { console.log('FRAME'); }, 50);
						}
					</script>
				</head>
				<body>
					<div id='imgdiv'>
						<img src=']] .. url
							:replace(' ', '%20')
							:replace('"', '%22')
							:replace("'", '%27')
							:replace('#', '%23')
							:replace('<', '%3C')
							:replace('=', '%3D'):replace('>', '%3E') .. [[' id='mainimage' />
					</div>
				</body>
			</html>]]
end

DSprays.HTMLPage = HTMLPage
DSprays.HTMLPageVideo = HTMLPageVideo

DSprays.URLQueue = DSprays.URLQueue or {}
DSprays.URLQueueVideo = DSprays.URLQueueVideo or {}
DSprays.URLQueueDynamic = DSprays.URLQueueDynamic or {}
local queue = DSprays.URLQueue
local queueVideo = DSprays.URLQueueVideo
local queueDynamic = DSprays.URLQueueDynamic
local coroutine_yield = coroutine.yield
local thinkPanels, thinkPanelsDynamic = {}, {}

do
	local rem1, rem2 = {}, {}

	for index, data in pairs(DSprays.TextureCacheVideo) do
		if IsValid(data.panel) then
			table.insert(thinkPanels, data)
		else
			rem1[index] = true
		end
	end

	for index, data in pairs(DSprays.TextureCacheDynamic) do
		if IsValid(data.panel) then
			table.insert(thinkPanelsDynamic, data)
		else
			rem2[index] = true
		end
	end

	for index in pairs(rem1) do
		DSprays.TextureCacheVideo[index] = nil
	end

	for index in pairs(rem2) do
		DSprays.TextureCacheDynamic[index] = nil
	end
end

DSprays.ThreadThinkSalt = (DSprays.ThreadThinkSalt or -1) + 1
local ThreadThinkSalt = DSprays.ThreadThinkSalt

local encoding_thread_busy = false

local function EncodeMaterial(material, width, height)
	encoding_thread_busy = true
	local clip_status = DisableClipping(true)

	local temp_copy_rt = GetRenderTarget(string.format('dsprays_copy_%d_%d', width, height), width, height)
	local temp_copy_rt_alpha = GetRenderTarget(string.format('dsprays_copy_alpha_%d_%d', width, height), width, height)
	local temp_copy_rt_final_alpha = GetRenderTarget(string.format('dsprays_copy_final_alpha_%d_%d', width, height), width, height)

	local temp_copy_rt_mat = CreateMaterial(string.format('dsprays_copy_%d_%d', width, height), 'UnlitGeneric', {
		['$translucent'] = '1',
		['$basetexture'] = string.format('!dsprays_copy_%d_%d', width, height)
	})

	local temp_copy_rt_alpha_mat = CreateMaterial(string.format('dsprays_copy_alpha_%d_%d', width, height), 'UnlitGeneric', {
		['$translucent'] = '1',
		['$basetexture'] = string.format('!dsprays_copy_alpha_%d_%d', width, height)
	})

	local temp_copy_rt_final_alpha_mat = CreateMaterial(string.format('dsprays_copy_final_alpha_%d_%d', width, height), 'UnlitGeneric', {
		['$translucent'] = '1',
		['$basetexture'] = string.format('!dsprays_copy_final_alpha_%d_%d', width, height)
	})

	temp_copy_rt_mat:SetTexture('$basetexture', temp_copy_rt)
	temp_copy_rt_alpha_mat:SetTexture('$basetexture', temp_copy_rt_alpha)
	temp_copy_rt_final_alpha_mat:SetTexture('$basetexture', temp_copy_rt_final_alpha)

	surface.SetDrawColor(255, 255, 255)

	local vtf = DLib.VTF.Create(2, width, height, IMAGE_FORMAT_DXT5, {fill = Color(0, 0, 0), mipmap_count = 1})

	-- capture HTML render
	render.PushRenderTarget(temp_copy_rt)
	render.Clear(0, 0, 0, 0, true, true)
	cam.Start2D()

	surface.SetMaterial(material)
	surface.DrawTexturedRect(0, 0, width, height)

	cam.End2D()

	-- write RGB channel
	-- write Alpha channel if if present (gmod update)
	vtf:CaptureRenderTargetCoroutine({
		pop_rendertarget = true
	})

	encoding_thread_busy = false
	return vtf
end

local function ThinkFunction(index)
	local panel
	local last_use = SysTime()

	coroutine_yield()

	while true do
		-- interrupt old thread
		if DSprays.ThreadThinkSalt ~= ThreadThinkSalt then
			if IsValid(panel) then
				panel:Remove()
			end

			return
		end

		if not queue[1] then
			coroutine_yield()

			if index > 4 and last_use < SysTime() and IsValid(panel) then
				panel:Remove()
				panel = nil
			end

			goto CONTINUE
		end

		local data = table.remove(queue, 1)
		local cached = not dsprays_bypass_cache:GetBool() and GetCache(data.index)
		local texture, newMat, insert_data, newMat2

		if not cached then
			if not IsValid(panel) then
				panel = vgui.Create('DHTML')
				panel:SetVisible(false)
				-- panel:SetPos(index * 20, 0)
			end

			last_use = SysTime() + 10

			if panel:GetTall() ~= data.height or panel:GetWide() ~= data.width then
				panel:SetSize(data.width, data.height)
			end

			panel:SetHTML(data.video and HTMLPageVideo(data.url, data.width, data.height) or HTMLPage(data.url, data.width, data.height))

			--panel:Refresh()

			if data.video then
				local setup = false

				function panel:ConsoleMessage(msg)
					if msg == 'SETUP' then
						setup = true
					end
				end

				while panel:IsLoading() do
					coroutine_yield()
				end

				local attempts = 200

				while not setup and attempts > 0 do
					attempts = attempts - 1
					coroutine_yield()
				end

				panel:UpdateHTMLTexture()

				coroutine.syswait(1)
			else
				local frames = 0

				function panel:ConsoleMessage(msg)
					if msg == 'FRAME' then
						frames = frames + 1
					end
				end

				while panel:IsLoading() do
					coroutine_yield()
				end

				while frames < 20 do
					coroutine_yield()
				end

				coroutine.syswait(1)
			end

			panel:UpdateHTMLTexture()

			local htmlmat = panel:GetHTMLMaterial()

			if not htmlmat then
				goto FINISH
			end

			texture = htmlmat:GetTexture('$basetexture')
			texture:Download()

			while encoding_thread_busy do
				coroutine_yield()
			end

			local vtf = EncodeMaterial(htmlmat, data.width, data.height)

			SetCache(data.index, vtf:ToString())
			texture = GetCache(data.index)
		else
			texture = cached
		end

		newMat = CreateMaterial('dsprays_' .. data.sha1, 'VertexLitGeneric', {
			['$basetexture'] = isstring(texture) and texture or texture:GetName(),
			['$vertexcolor'] = 1,
			['$vertexalpha'] = 1,
			['$nolod'] = 1,
			['$nocull'] = 1,
		})

		newMat2 = CreateMaterial('dsprays_unlit_' .. data.sha1, 'UnlitGeneric', {
			['$basetexture'] = isstring(texture) and texture or texture:GetName(),
			['$vertexcolor'] = 1,
			['$vertexalpha'] = 1,
			['$nolod'] = 1,
		})

		newMat:SetTexture('$basetexture', texture)
		newMat2:SetTexture('$basetexture', texture)

		if isstring(texture) then
			newMat:GetTexture('$basetexture'):Download()
			newMat2:GetTexture('$basetexture'):Download()
		end

		insert_data = {
			texture = texture,
			material = newMat,
			material_gui = newMat2,
			url = data.url,
			width = data.width,
			height = data.height,
		}

		DSprays.TextureCache[data.index] = insert_data
		DSprays.PendingTextures[data.index] = nil

		for i, resolve in ipairs(data.resolve) do
			resolve(insert_data)
		end

		::FINISH::

		for i = 1, 10 do
			coroutine_yield()
		end

		::CONTINUE::
	end
end

local function ThinkFunctionVideo()
	while true do
		-- interrupt old thread
		if DSprays.ThreadThinkSalt ~= ThreadThinkSalt then return end

		if not queueVideo[1] then
			coroutine_yield()
			goto CONTINUE
		end

		local data = table.remove(queueVideo, 1)

		local panel = vgui.Create('DHTML')
		panel:SetVisible(false)
		panel:SetSize(data.width, data.height)
		panel:SetHTML(HTMLPageVideo(data.url, data.width, data.height))
		panel:Refresh()

		local setup = false

		function panel:ConsoleMessage(msg)
			if msg == 'SETUP' then
				setup = true
			end
		end

		while panel:IsLoading() do
			coroutine_yield()
		end

		local attempts = 200

		while not setup and attempts > 0 do
			attempts = attempts - 1
			coroutine_yield()
		end

		-- coroutine.syswait(1)

		panel:UpdateHTMLTexture()

		local htmlmat = panel:GetHTMLMaterial()
		local texture, newMat, insert_data, newMat2

		local attempts = 100

		while not htmlmat and attempts > 0 and panel:IsValid() do
			attempts = attempts - 1
			htmlmat = panel:GetHTMLMaterial()
			coroutine_yield()
		end

		if not htmlmat then
			if panel:IsValid() then
				panel:Remove()
			end

			goto CONTINUE
		end

		texture = htmlmat:GetTexture('$basetexture')
		-- texture:Download()

		newMat = CreateMaterial(htmlmat:GetName() .. '_dsprays', 'VertexLitGeneric', {
			['$basetexture'] = texture:GetName(),
			['$vertexcolor'] = 1,
			['$vertexalpha'] = 1,
			['$nolod'] = 1,
			['$nocull'] = 1,
		})

		newMat2 = CreateMaterial(htmlmat:GetName() .. '_dsprays_ulit', 'UnlitGeneric', {
			['$basetexture'] = texture:GetName(),
			['$vertexcolor'] = 1,
			['$vertexalpha'] = 1,
			['$nolod'] = 1,
		})

		newMat:SetTexture('$basetexture', texture)
		newMat2:SetTexture('$basetexture', texture)

		insert_data = {
			texture = texture,
			material = newMat,
			material_gui = newMat2,
			url = data.url,
			width = data.width,
			height = data.height,
			panel = panel,
		}

		DSprays.TextureCacheVideo[data.index] = insert_data
		DSprays.PendingTexturesVideo[data.index] = nil
		table.insert(thinkPanels, insert_data)

		for i, resolve in ipairs(data.resolve) do
			resolve(insert_data)
		end

		::CONTINUE::
	end
end

local function ThinkFunctionDynamicPicture()
	while true do
		-- interrupt old thread
		if DSprays.ThreadThinkSalt ~= ThreadThinkSalt then return end

		if not queueDynamic[1] then
			coroutine_yield()
			goto CONTINUE
		end

		local data = table.remove(queueDynamic, 1)

		local panel = vgui.Create('DHTML')
		panel:SetVisible(false)
		panel:SetSize(data.width, data.height)
		panel:SetHTML(HTMLPage(data.url, data.width, data.height))
		panel:Refresh()

		local frames = 0

		function panel:ConsoleMessage(msg)
			if msg == 'FRAME' then
				frames = frames + 1
			end
		end

		while panel:IsLoading() do
			coroutine_yield()
		end

		while frames < 20 do
			coroutine_yield()
		end

		coroutine.syswait(1)

		panel:UpdateHTMLTexture()

		local texture, newMat, insert_data, newMat2
		local attempts = 200
		local htmlmat = panel:GetHTMLMaterial()

		while not htmlmat and attempts > 0 do
			attempts = attempts - 1
			htmlmat = panel:GetHTMLMaterial()
			coroutine_yield()
		end

		if not htmlmat then
			if panel:IsValid() then
				panel:Remove()
			end

			DSprays.PendingTexturesDynamic[data.index] = nil

			for i, reject in ipairs(data.reject) do
				reject(insert_data)
			end

			goto CONTINUE
		end

		texture = htmlmat:GetTexture('$basetexture')
		-- texture:Download()

		newMat = CreateMaterial(htmlmat:GetName() .. '_dsprays', 'VertexLitGeneric', {
			['$basetexture'] = texture:GetName(),
			['$vertexcolor'] = 1,
			['$vertexalpha'] = 1,
			['$nolod'] = 1,
			['$nocull'] = 1,
		})

		newMat2 = CreateMaterial(htmlmat:GetName() .. '_dsprays_ulit', 'UnlitGeneric', {
			['$basetexture'] = texture:GetName(),
			['$vertexcolor'] = 1,
			['$vertexalpha'] = 1,
			['$nolod'] = 1,
		})

		newMat:SetTexture('$basetexture', texture)
		newMat2:SetTexture('$basetexture', texture)

		insert_data = {
			texture = texture,
			material = newMat,
			material_gui = newMat2,
			url = data.url,
			width = data.width,
			height = data.height,
			panel = panel,
		}

		DSprays.TextureCacheDynamic[data.index] = insert_data
		DSprays.PendingTexturesDynamic[data.index] = nil
		table.insert(thinkPanelsDynamic, insert_data)

		for i, resolve in ipairs(data.resolve) do
			resolve(insert_data)
		end

		::CONTINUE::
	end
end

local thinkThreadVideo1     = DSprays.thinkThreadVideo1     or coroutine.create(ThinkFunctionVideo)
local thinkThreadDynamic1   = DSprays.thinkThreadDynamic1   or coroutine.create(ThinkFunctionDynamicPicture)

DSprays.PlainTextureThinkThreads = DSprays.PlainTextureThinkThreads or {}

for i = 1, 16 do
	if not DSprays.PlainTextureThinkThreads[i] then
		DSprays.PlainTextureThinkThreads[i] = coroutine.create(ThinkFunction)
		coroutine.resume(DSprays.PlainTextureThinkThreads[i], i)
	end
end

DSprays.thinkThreadVideo1   = thinkThreadVideo1
DSprays.thinkThreadDynamic1 = thinkThreadDynamic1

local coroutine_resume = coroutine.resume
local coroutine_status = coroutine.status
local SysTime = SysTime
local FrameNumber = FrameNumber

local function PreRender()
	local frame = FrameNumber() - 1

	for i, data in ipairs(thinkPanels) do
		if not IsValid(data.panel) then
			table.remove(thinkPanels, i)
			return
		end

		if not data.feedback or data.feedback == frame then
			if not data.paused and not data._force_paused then
				data.panel:UpdateHTMLTexture()
			end

			if data.force_paused and not data._force_paused then
				data._force_paused = true
				data.panel:RunJavascript([[document.getElementById('mainimage').pause();]])
			elseif data._force_paused and not data.force_paused then
				data._force_paused = false
				data.panel:RunJavascript([[document.getElementById('mainimage').play();]])
			end

			if data.paused then
				if not data.force_paused then
					data.panel:RunJavascript([[document.getElementById('mainimage').play();]])
				end

				data.paused = false
			end

			if not data.paused and not data._force_paused and data.volume ~= data._volume then
				if not data._volume or data._volume == 0 then
					data.panel:RunJavascript([[document.getElementById('mainimage').muted = false;]])
				elseif data._volume and data._volume > 0 and math.floor(data.volume) == 0 then
					data.panel:RunJavascript([[document.getElementById('mainimage').muted = true;]])
				end

				data._volume = math.floor(data.volume)
				data.panel:RunJavascript(string.format([[document.getElementById('mainimage').volume = %.2f;]], math.clamp(data._volume / 100, 0, 1)))
			end
		elseif not data.paused then
			if not data.force_paused then
				data.panel:RunJavascript([[document.getElementById('mainimage').pause();]])
			end

			data.paused = true
		end
	end

	for i, data in ipairs(thinkPanelsDynamic) do
		if not IsValid(data.panel) then
			table.remove(thinkPanelsDynamic, i)
			return
		end

		if not data.feedback or data.feedback == frame then
			data.panel:UpdateHTMLTexture()

			if data.paused then
				data.panel:RunJavascript([==[document.getElementById('mainimage').style['display'] = '';]==])
				data.paused = false
			end
		elseif not data.paused then
			data.panel:RunJavascript([==[document.getElementById('mainimage').style['display'] = 'none';]==])
			data.paused = true
		end
	end
end

local function Think()
	for i = 1, 16 do
		local thread = DSprays.PlainTextureThinkThreads[i]

		if coroutine_status(thread) == 'dead' then
			thread = coroutine.create(ThinkFunction)
			coroutine_resume(thread, i)
			DSprays.PlainTextureThinkThreads[i] = thread
		end

		local status, err = coroutine_resume(thread)

		if not status then
			ErrorNoHalt('[DSprays] DSprays URL Processor thread ' .. i .. ' died!\n')
			ErrorNoHalt('[DSprays] ' .. err .. '\n')
			ErrorNoHalt('[DSprays] Materials might be left in corrupted state.\n')

			thread = coroutine.create(ThinkFunction)
			coroutine_resume(thread, i)
			DSprays.PlainTextureThinkThreads[i] = thread
		end
	end

	if coroutine_status(thinkThreadVideo1) == 'dead' then
		thinkThreadVideo1 = coroutine.create(ThinkFunctionVideo)
		DSprays.thinkThreadVideo1 = thinkThreadVideo1
	end

	local status, err = coroutine_resume(thinkThreadVideo1)

	if not status then
		ErrorNoHalt('[DSprays] DSprays URL Video Processor thread 1 died!\n')
		ErrorNoHalt('[DSprays] ' .. err .. '\n')
		ErrorNoHalt('[DSprays] Materials might be left in corrupted state.\n')

		thinkThreadVideo1 = coroutine.create(ThinkFunctionVideo)
		DSprays.thinkThreadVideo1 = thinkThreadVideo1
	end

	if coroutine_status(thinkThreadDynamic1) == 'dead' then
		thinkThreadDynamic1 = coroutine.create(ThinkFunctionDynamicPicture)
		DSprays.thinkThreadDynamic1 = thinkThreadDynamic1
	end

	status, err = coroutine_resume(thinkThreadDynamic1)

	if not status then
		ErrorNoHalt('[DSprays] DSprays URL Dynamic Picture Processor thread 1 died!\n')
		ErrorNoHalt('[DSprays] ' .. err .. '\n')
		ErrorNoHalt('[DSprays] Materials might be left in corrupted state.\n')

		thinkThreadDynamic1 = coroutine.create(ThinkFunctionDynamicPicture)
		DSprays.thinkThreadDynamic1 = thinkThreadDynamic1
	end
end

function DSprays.StaticURLTextureInstant(url, width, height)
	local index = width .. '_' .. height .. '_' .. url

	if DSprays.TextureCache[index] then
		return DSprays.TextureCache[index]
	end

	if DSprays.PendingTextures[index] then
		return
	end

	DSprays.StaticURLTexture(url, width, height)
end

function DSprays.StaticURLTexture(url, width, height)
	local index = width .. '_' .. height .. '_' .. url

	if DSprays.TextureCache[index] then
		return Promise(function(resolve, reject)
			resolve(DSprays.TextureCache[index])
		end)
	end

	if DSprays.PendingTextures[index] then
		return Promise(function(resolve, reject)
			table.insert(DSprays.PendingTextures[index].resolve, resolve)
			table.insert(DSprays.PendingTextures[index].reject, reject)
		end)
	end

	return Promise(function(resolve, reject)
		local data = {
			resolve = {resolve},
			reject = {reject},
			url = url,
			width = width,
			height = height,
			index = index,
			sha1 = DLib.Util.QuickSHA1(index),
		}

		DSprays.PendingTextures[index] = data

		table.insert(queue, data)
	end)
end

function DSprays.DynamicURLTexture(url, width, height)
	local index = width .. '_' .. height .. '_' .. url

	if DSprays.TextureCacheDynamic[index] then
		return Promise(function(resolve, reject)
			resolve(DSprays.TextureCacheDynamic[index])
		end)
	end

	if DSprays.PendingTexturesDynamic[index] then
		return Promise(function(resolve, reject)
			table.insert(DSprays.PendingTexturesDynamic[index].resolve, resolve)
			table.insert(DSprays.PendingTexturesDynamic[index].reject, reject)
		end)
	end

	return Promise(function(resolve, reject)
		local data = {
			resolve = {resolve},
			reject = {reject},
			url = url,
			width = width,
			height = height,
			index = index,
			sha1 = DLib.Util.QuickSHA1(index),
		}

		DSprays.PendingTexturesDynamic[index] = data

		table.insert(queueDynamic, data)
	end)
end

function DSprays.VideoAsStaticURLTextureInstant(url, width, height)
	local index = width .. '_' .. height .. '_' .. url .. '_video'

	if DSprays.TextureCache[index] then
		return DSprays.TextureCache[index]
	end

	if DSprays.PendingTextures[index] then
		return
	end

	DSprays.VideoAsStaticURLTexture(url, width, height)
end

function DSprays.VideoAsStaticURLTexture(url, width, height)
	local index = width .. '_' .. height .. '_' .. url .. '_video'

	if DSprays.TextureCache[index] then
		return Promise(function(resolve, reject)
			resolve(DSprays.TextureCache[index])
		end)
	end

	if DSprays.PendingTextures[index] then
		return Promise(function(resolve, reject)
			table.insert(DSprays.PendingTextures[index].resolve, resolve)
			table.insert(DSprays.PendingTextures[index].reject, reject)
		end)
	end

	return Promise(function(resolve, reject)
		local data = {
			resolve = {resolve},
			reject = {reject},
			url = url,
			width = width,
			height = height,
			index = index,
			sha1 = DLib.Util.QuickSHA1(index),
			video = true,
		}

		DSprays.PendingTextures[index] = data

		table.insert(queue, data)
	end)
end

function DSprays.VideoURLTexture(url, width, height)
	local index = width .. '_' .. height .. '_' .. url

	if DSprays.TextureCacheVideo[index] then
		return Promise(function(resolve, reject)
			resolve(DSprays.TextureCacheVideo[index])
		end)
	end

	if DSprays.PendingTextures[index] then
		return Promise(function(resolve, reject)
			table.insert(DSprays.PendingTexturesVideo[index].resolve, resolve)
			table.insert(DSprays.PendingTexturesVideo[index].reject, reject)
		end)
	end

	return Promise(function(resolve, reject)
		local data = {
			resolve = {resolve},
			reject = {reject},
			url = url,
			width = width,
			height = height,
			index = index,
			sha1 = DLib.Util.QuickSHA1(index),
		}

		DSprays.PendingTexturesVideo[index] = data

		table.insert(queueVideo, data)
	end)
end

hook.Add('Think', 'DSprays URL Processor thread', Think)
hook.Add('PreRender', 'DSprays Tick Videos', PreRender)
hook.Add('InvalidateMaterialCache', 'DSprays Clear Texture Cache', InvalidateMaterialCache)
hook.Add('DSprays_BypassCacheChanged', 'DSprays Clear Texture Cache', InvalidateMaterialCache)
