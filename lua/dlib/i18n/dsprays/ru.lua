
-- Copyright (C) 2020-2021 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.


gui.dsrpays.main.title = 'Спреи'
gui.dsrpays.main.labels.label = 'Надпись'
gui.dsrpays.main.labels.url = 'URL'
gui.dsrpays.main.labels.bottom = 'Флаги'

gui.dsrpays.main.labels.main = 'Главный в наборе'
gui.dsrpays.main.labels.hide = 'Скрыть'
gui.dsrpays.main.labels.random = 'Случайный выбор'
gui.dsrpays.main.labels.nsfw = 'NSFW'
gui.dsrpays.main.labels.video = 'WebM/Видео'
gui.dsrpays.main.labels.unique_indexing = 'Уникальные индексы спреев'
gui.dsrpays.main.labels.unique_indexing_tip = 'Позволяет наносить спреи из данного набора отдельно\nЕсли на сервере разрешено наносить несколько спреев одновременно'
gui.dsrpays.main.labels.mass_import = 'Массовый ввод URL'
gui.dsrpays.main.labels.mass_import_tip = 'Введите список URL для массового добавления\nКаждый URL вставляется на отдельной строке'
gui.dsrpays.main.remove_spray = 'Удалить'
gui.dsrpays.main.background_color = 'Фоновый цвет'
gui.dsrpays.main.background_color_enable = 'Активен'
gui.dsrpays.main.types.type = 'Тип:'
gui.dsrpays.main.types.static = 'Статичное изображение'
gui.dsrpays.main.types.dynamic = 'APNG/GIF/Анимированный WebP/Анимированный AVIF'
gui.dsrpays.main.types.dynamic_video = 'WebM'

gui.dsrpays.main.spray_size = 'Размер'

gui.dsrpays.missing.notification = 'Этот сервер использует DSprays. Судя по всему, у вас нет изображений в каких либо наборах DSprays, используйте консольную команду dsprays_gui для добавления таковых. Иначе DSprays будет использовать ваш стандартный спрей. Данное сообщение будет выводиться только один раз в каждой игровой сессии.'

gui.dsrpays.missing.missing = 'Спреи отстутствуют'
gui.dsrpays.missing.missing_desc = 'Главный набор пуст. Используйте консольную команду `dsprays_gui` что бы добавить спреи.'

gui.dsrpays.main.labels.randomied = 'Перемешан'

gui.dsrpays.tabs.galleries = 'Наборы'
gui.dsrpays.tabs.settings = 'Настройки'
gui.dsrpays.tabs.server_settings = 'Серверные настройки'

gui.dsrpays.settings.rating = 'Максимальный возрастной рейтинг'
gui.dsrpays.settings.fullbright = 'Отключить просчёт освещение у спреев'
gui.dsrpays.settings.size = 'Разрешение текстуры спреев в мире'
gui.dsrpays.settings.size_video = 'Разрешение видео спреев в мире'
gui.dsrpays.settings.hide_everything = 'Спрятать все спреи'
gui.dsrpays.settings.hide_unmatching = 'Спрятать спреи не соотвествующие настройкам\nвместо пикселяции'
gui.dsrpays.settings.bypass_cache = 'Не использовать кеш'
gui.dsrpays.settings.do_not_animate = 'Анимировать спреи по умолчанию'
gui.dsrpays.settings.reload_materials = 'Выполнить dlib_reload_materials'

gui.dsrpays.misc.main_gallery = 'Основной набор'
gui.dsrpays.misc.create_gallery = 'Создать набор'
gui.dsrpays.misc.create_gallery_desc = 'Укажите имя нового набора'
gui.dsrpays.misc.delete_gallery = 'Удалить набор'
gui.dsrpays.misc.delete_gallery_desc = 'Вы действительно хотите удалить данный набор?\nДанное действие нельзя отменить!'
gui.dsrpays.misc.rename_gallery = 'Переименовать набор'
gui.dsrpays.misc.rename_gallery_desc = 'Введите новое название набора'

gui.dsrpays.misc.source_spray_missing = 'Спрей движка Source\nне найден в файлах загрузок'

gui.dsrpays.binding.bad_binding = 'Клавиша нанесения спреев не найдена'
gui.dsrpays.binding.bad_binding_desc = 'Невозможно определить клавишу бинда спреев, пожалуйста, перебиндьте на другую клавишу.'

gui.dsrpays.binding.gallery_is_empty = 'Набор пуст'
gui.dsrpays.binding.gallery_is_empty_desc = 'В данном наборе нет ни одного изображения'

gui.dsrpays.rating.rating = 'Возрастной рейтинг'

gui.dsrpays.rating.nsfw = 'NSFW'
gui.dsrpays.rating.safe = 'Безопасно (G)'
gui.dsrpays.rating.suggestive = 'Смутительно (PG-13)'
gui.dsrpays.rating.questionable = 'Откровенно (17+)'
gui.dsrpays.rating.explicit = 'Для взрослых (18+)'

gui.dsrpays.controls.title = 'Спрей'
gui.dsrpays.controls.label = 'Надпись:'
gui.dsrpays.controls.sprayed_by = 'Нарисовано:'
gui.dsrpays.controls.created_by = 'Плакат создан:'
gui.dsrpays.controls.video = 'Управление видео'
gui.dsrpays.controls.video_pause = 'Пауза'
gui.dsrpays.controls.animate = 'Анимировать'
gui.dsrpays.controls.volume = 'Громкость'
gui.dsrpays.controls.delete = 'Удалить'
gui.dsrpays.controls.delete_cppi = 'Удалить (как владелец сущности)'
gui.dsrpays.controls.deleted = 'Ваш спрей %q удалён'
gui.dsrpays.controls.deleted_cppi = 'Спрей %q удалён'
gui.dsrpays.controls.deleted_cppi2 = 'Спрей %q игрока #E is удалён'
gui.dsrpays.controls.admin_delete = 'Удалить (как администратор)'
gui.dsrpays.controls.admin_deleted = 'Спрей %q удалён'
gui.dsrpays.controls.admin_deleted2 = 'Спрей %q игрока #E удалён'
gui.dsrpays.controls.admin_deleted_log = '#E удалил спрей %q игрока'
gui.dsrpays.controls.admin_deleted_log2 = '#E удалил спрей %q игрока #E'
gui.dsrpays.controls.admin_get_url = 'Копировать URL'

gui.dsrpays.controls.hide_url = 'Спрятать по URL'
gui.dsrpays.controls.hide_author = 'Спрятать по автору'

gui.dsrpays.misc.url_hidden = 'URL скрыт'
gui.dsrpays.misc.author_hidden = 'Автор скрыт'

gui.tool.dsprays_tool.name = 'Инструмент плакатов'
gui.tool.dsprays_tool.desc = 'Позволяет создавать плакаты'
gui.undone.dsprays_poster = 'Отмена плаката DSprays'
gui.tool.dsprays_tool.left = 'Для создания нового плаката или обновления настроек существующего'
gui.tool.dsprays_tool.right = 'Для копирования настроек плаката, включая его URL'

gui.cami.dsprays.dsprays_admin = 'Контролирует, может ли эта группа удалять спреи других игроков'

gui.dsrpays.tool.dont_move = 'Обездвижить'
gui.dsrpays.tool.dont_collide = 'Отключить коллизии'

gui.dsprays.dxt_notify = 'DSprays сжимает DXT5 текстуру'
gui.dsprays.cache_size = 'DSprays занимает %s дискового пространства для кеша текстур (он управляется и очищается автоматически)'
