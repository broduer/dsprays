
-- Copyright (C) 2020-2021 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.


gui.dsrpays.main.title = 'Sprays'
gui.dsrpays.main.labels.label = 'Label'
gui.dsrpays.main.labels.url = 'URL'
gui.dsrpays.main.labels.bottom = 'Flags'

gui.dsrpays.main.labels.main = 'Main in gallery'
gui.dsrpays.main.labels.hide = 'Disable'
gui.dsrpays.main.labels.random = 'Shuffle'
gui.dsrpays.main.labels.nsfw = 'NSFW'
gui.dsrpays.main.labels.video = 'WebM/Video'
gui.dsrpays.main.labels.unique_indexing = 'Unique spray indexes'
gui.dsrpays.main.labels.unique_indexing_tip = 'Allows to spray down each picture separately from this gallery\nIf server has multiple sprays enabled'
gui.dsrpays.main.labels.mass_import = 'Mass import URLs'
gui.dsrpays.main.labels.mass_import_tip = 'Input URLs for mass import\nEach URL is separated by newline'
gui.dsrpays.main.remove_spray = 'Remove'
gui.dsrpays.main.background_color = 'Background color'
gui.dsrpays.main.background_color_enable = 'Enable'
gui.dsrpays.main.types.type = 'Spray type:'
gui.dsrpays.main.types.static = 'Static picture'
gui.dsrpays.main.types.dynamic = 'APNG/GIF/Animated WebP/Animated AVIF'
gui.dsrpays.main.types.dynamic_video = 'WebM'

gui.dsrpays.main.spray_size = 'Size'

gui.dsrpays.missing.missing = 'No sprays defined'
gui.dsrpays.missing.missing_desc = 'No sprays defined in main gallery. Use `dsprays_gui` console command to define some.'

gui.dsrpays.missing.notification = 'This server use DSprays. You have no sprays defined in DSprays galleries, use dsprays_gui to define some. Without it, DSprays will use your Source Engine spray. This message will appear only once per session.'

gui.dsrpays.main.labels.randomied = 'Shuffled'

gui.dsrpays.tabs.galleries = 'Galleries'

gui.dsrpays.tabs.settings = 'Settings'
gui.dsrpays.tabs.server_settings = 'Server settings'

gui.dsrpays.settings.rating = 'Maximal age rating'
gui.dsrpays.settings.fullbright = 'Fullbright sprays'
gui.dsrpays.settings.size = 'In-world texture resolution'
gui.dsrpays.settings.size_video = 'In-world video resolution'
gui.dsrpays.settings.hide_everything = 'Hide everything'
gui.dsrpays.settings.hide_unmatching = 'Hide unmatching instead of pixelating'
gui.dsrpays.settings.bypass_cache = 'Bypass cache files'
gui.dsrpays.settings.do_not_animate = 'Animate sprays by default'
gui.dsrpays.settings.reload_materials = 'Execute dlib_reload_materials'

gui.dsrpays.misc.main_gallery = 'Main gallery'
gui.dsrpays.misc.create_gallery = 'Create gallery'
gui.dsrpays.misc.create_gallery_desc = 'Specify new gallery name'
gui.dsrpays.misc.delete_gallery = 'Delete gallery'
gui.dsrpays.misc.delete_gallery_desc = 'Do you really want to delete this gallery?\nThis action is permanent!'
gui.dsrpays.misc.rename_gallery = 'Rename gallery'
gui.dsrpays.misc.rename_gallery_desc = 'Type in gallery new name'

gui.dsrpays.misc.source_spray_missing = 'Source Engine spray is missing\nfrom downloads folder'

gui.dsrpays.binding.bad_binding = 'Spray binding not found'
gui.dsrpays.binding.bad_binding_desc = 'Unable to find spray binding button key code, please, rebind spray command.'

gui.dsrpays.binding.gallery_is_empty = 'Gallery is empty'
gui.dsrpays.binding.gallery_is_empty_desc = 'This gallery contain no images!'

gui.dsrpays.rating.rating = 'Age rating'

gui.dsrpays.rating.nsfw = 'NSFW'
gui.dsrpays.rating.safe = 'Safe (G)'
gui.dsrpays.rating.suggestive = 'Suggestive (PG-13)'
gui.dsrpays.rating.questionable = 'Questionable (17+)'
gui.dsrpays.rating.explicit = 'Mature (explicit/18+)'

gui.dsrpays.controls.title = 'Spray controls'
gui.dsrpays.controls.label = 'Label:'
gui.dsrpays.controls.sprayed_by = 'Sprayed by:'
gui.dsrpays.controls.created_by = 'Poster by:'
gui.dsrpays.controls.video = 'Video controls'
gui.dsrpays.controls.video_pause = 'Pause'
gui.dsrpays.controls.animate = 'Animated'
gui.dsrpays.controls.volume = 'Volume'
gui.dsrpays.controls.delete = 'Delete'
gui.dsrpays.controls.delete_cppi = 'Delete (as entity owner)'
gui.dsrpays.controls.deleted = 'Your spray %q is now deleted'
gui.dsrpays.controls.deleted_cppi = 'Spray %q is now deleted'
gui.dsrpays.controls.deleted_cppi2 = 'Spray %q of #E is now deleted'
gui.dsrpays.controls.admin_delete = 'Delete (as admin)'
gui.dsrpays.controls.admin_deleted = 'Spray %q deleted'
gui.dsrpays.controls.admin_deleted2 = 'Spray %q by #E is now deleted'
gui.dsrpays.controls.admin_deleted_log = '#E deleted a spray %q'
gui.dsrpays.controls.admin_deleted_log2 = '#E deleted a spray %q by #E'
gui.dsrpays.controls.admin_get_url = 'Copy URL'

gui.dsrpays.controls.hide_url = 'Hide by URL'
gui.dsrpays.controls.hide_author = 'Hide by user'

gui.dsrpays.misc.url_hidden = 'URL hidden'
gui.dsrpays.misc.author_hidden = 'User hidden'

gui.tool.dsprays_tool.name = 'Poster Tool'
gui.tool.dsprays_tool.desc = 'Allows to put down poster images'
gui.undone.dsprays_poster = 'Undone DSprays Poster'
gui.tool.dsprays_tool.left = 'To put down a poster or update settings of existing one'
gui.tool.dsprays_tool.right = "To copy poster's settings, including it's URL"

gui.cami.dsprays.dsprays_admin = 'Whenever can this group remove sprays of other players'

gui.dsrpays.tool.dont_move = 'Disable movement'
gui.dsrpays.tool.dont_collide = 'Disable collisions'

gui.dsprays.dxt_notify = 'DSprays is encoding DXT5 texture'
gui.dsprays.cache_size = 'DSprays take %s of disk space for texture cache (it is managed automatically)'
