
-- Copyright (C) 2020-2021 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

include('shared.lua')

DEFINE_BASECLASS(ENT.Base)

function ENT:OnRemove()
	BaseClass.OnRemove(self)

	if IsValid(self._mesh) then
		self._mesh:Destroy()
	end
end

local color_white = Color()
local normal = Vector(0, 0, 1)

function ENT:Draw()
	local hl = IsValid(DSprays.highlight_panel)
	if hl and self.highlight_panel ~= DSprays.highlight_panel then return end

	return BaseClass.Draw(self, hl)
end

function ENT:Rebuild()
	if not DSprays.ValidURL(self:GetSprayURL()) then return false end

	if not IsValid(self.spray_object) then
		self.spray_object = DSprays.MeshObject(self:GetPos(), self:GetAngles())
	end

	self._dirty_rev = self:GetRev()

	local obj = self.spray_object
	local rating = self:GetSprayRating()
	local hide_url = DSprays.IsURLHidden(self:GetSprayURL())
	local hide_author = self:GetSpraySteamID() ~= '' and DSprays.IsSteamIDHidden(self:GetSpraySteamID())

	local hide = self:DetermineHide(rating, hide_url, hide_author, obj)

	if hide then
		self:SetNoDraw(true)
		return
	end

	obj:SetBackgroundColor(not hide_url and self:GetSprayBackgroundC() or nil)
	self:SetNoDraw(false)

	obj:SetMaterial(DSprays.LoadingMaterial)

	if self._mesh_size ~= self:GetSpraySize() then
		self._mesh_size = self:GetSpraySize() / 2

		local bottom_left = Vector(-self._mesh_size, -self._mesh_size)
		local bottom_right = Vector(self._mesh_size, -self._mesh_size)
		local top_left = Vector(-self._mesh_size, self._mesh_size)
		local top_right  = Vector(self._mesh_size, self._mesh_size)

		self:SetRenderBounds(Vector(-self._mesh_size, -self._mesh_size, -4), Vector(self._mesh_size, self._mesh_size, 4))
		debugoverlay.BoxAngles(self:GetPos(), Vector(-self._mesh_size, -self._mesh_size, -4), Vector(self._mesh_size, self._mesh_size, 4), self:GetAngles(), 4, Color(86, 255, 114, 50))

		local mesh = Mesh()

		mesh:BuildFromTriangles({
			{
				pos = top_right,
				color = color_white,
				u = 1,
				v = 0,
				normal = normal,
			},

			{
				pos = bottom_left,
				color = color_white,
				u = 0,
				v = 1,
				normal = normal,
			},

			{
				pos = top_left,
				color = color_white,
				u = 0,
				v = 0,
				normal = normal,
			},

			{
				pos = top_right,
				color = color_white,
				u = 1,
				v = 0,
				normal = normal,
			},

			{
				pos = bottom_right,
				color = color_white,
				u = 1,
				v = 1,
				normal = normal,
			},

			{
				pos = bottom_left,
				color = color_white,
				u = 0,
				v = 1,
				normal = normal,
			}
		})

		obj:SetIMesh(mesh)
		if IsValid(self._mesh) then self._mesh:Destroy() end
		self._mesh = mesh
	end

	if not hide_url and not hide_author then
		self:LoadURL(self:GetSprayURL(), rating, obj)
	else
		obj:SetMaterial()
	end
end
