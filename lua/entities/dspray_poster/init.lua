
-- Copyright (C) 2020-2021 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

AddCSLuaFile('shared.lua')
AddCSLuaFile('cl_init.lua')

include('shared.lua')

function ENT:Initialize()
	self:SetModel('models/hunter/plates/plate1x1.mdl')
	self:PhysicsInit(SOLID_VPHYSICS)

	self:UpdatePhysics()
end

function ENT:UpdatePhysics()
	if self:GetSprayDontCollide() then
		self:SetCollisionGroup(COLLISION_GROUP_WORLD)
	else
		self:SetCollisionGroup(COLLISION_GROUP_NONE)
	end

	local phys = self:GetPhysicsObject()

	if IsValid(phys) then
		-- WELL I COULD SetMoveType(MOVETYPE_NONE)
		-- but it fucking doesn't work with duplicators
		-- because when movetype_none is set somewhere before spawn
		-- you can't interact with prop at all despite being solid

		phys:EnableMotion(not self:GetSprayDontMove())
		phys:EnableCollisions(not self:GetSprayDontCollide())
	end
end

hook.Add('PhysgunDrop', 'DSprays Poster Physics Handler', function(ply, ent)
	if ent:GetClass() ~= 'dspray_poster' then return end

	if ent:GetSprayDontMove() then
		local phys = ent:GetPhysicsObject()

		if phys:IsValid() then
			phys:EnableMotion(false)
		end
	end
end)

local function factory(ply, data)
	if not ply:CheckLimit('dsprays_posters') then return false end

	local ent = ents.Create('dspray_poster')

	duplicator.DoGeneric(ent, data)
	duplicator.DoGenericPhysics(ent, pl, data)

	if istable(data.DT) then
		data.DT.SpraySteamID = ply:SteamID() or 'STEAM_0:0:0'
		data.DT.Rev = 0
	end

	ply:AddCleanup('dsprays_posters', ent)
	ply:AddCount('dsprays_posters', ent)

	ent:Spawn()

	return ent
end

duplicator.RegisterEntityClass('dspray_poster', factory, 'Data')
