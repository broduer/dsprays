
-- Copyright (C) 2020-2021 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

ENT.PrintName = 'Base DSprays Entity'
ENT.Type = 'anim'
ENT.Author = 'DBotThePony'
ENT.Spawnable = false

function ENT:IsStaticPicture()
	return self:GetSprayType() == DSprays.TYPE_STATIC_PICTURE
end

function ENT:IsDynamicPicture()
	return self:GetSprayType() == DSprays.TYPE_DYNAMIC_PICTURE
end

function ENT:IsDynamicVideo()
	return self:GetSprayType() == DSprays.TYPE_DYNAMIC_VIDEO
end

function ENT:SetupDataTables()
	self:NetworkVar('String', 0, 'SpraySteamID')
	self:NetworkVar('String', 1, 'SprayURL')

	self:NetworkVar('Int', 0, 'SpraySize')
	self:NetworkVar('Int', 1, 'SprayRating')
	self:NetworkVar('Int', 2, 'SprayBackgroundR')
	self:NetworkVar('Int', 3, 'SprayBackgroundG')
	self:NetworkVar('Int', 4, 'SprayBackgroundB')
	self:NetworkVar('Int', 5, 'SprayBackgroundA')

	self:NetworkVar('Int', 6, 'Rev')

	self:NetworkVar('Int', 7, 'SprayType')
	self:NetworkVar('String', 2, 'SprayLabel')

	self:NetworkVar('Bool', 1, 'SprayBackground')
end

function ENT:LoadDataFromTable(data)
	self:SetSprayRating(data.rating or 1)

	self:SetSprayBackground(IsColor(data.background))

	if data.background then
		self:SetSprayBackgroundR(data.background.r)
		self:SetSprayBackgroundG(data.background.g)
		self:SetSprayBackgroundB(data.background.b)
		self:SetSprayBackgroundA(data.background.a)
	end

	self:SetSprayURL(data.url)
	self:SetSprayType(data.spray_type)

	self:SetSprayLabel(data.label)
	self:SetSpraySize(data.size)
end

function ENT:GetSprayBackgroundC()
	if self:GetSprayBackground() then
		return Color(self:GetSprayBackgroundR(), self:GetSprayBackgroundG(), self:GetSprayBackgroundB(), self:GetSprayBackgroundA())
	end
end

function ENT:GetSprayPlayer()
	return player.GetBySteamID(self:GetSpraySteamID())
end
